#include <Arduino.h>
#include <LiquidCrystal.h>
#include <Servo.h>
#include <protofunciones.h>
#include <Keypad.h>
//#include <WiFi.h>
#include <ThingerESP32.h>
#include <FirebaseESP32.h>

//Defino unidades que usare para la funcion de checkeo de enlace wifi
unsigned char contador_segundos = 0; 
char flag_while = 0;

ThingerESP32 thing(user, id_device, credential); //Creo un objeto para el enlace Wifi con thinger.io
LiquidCrystal lcd(PB4, PB5, PB6, PB7, PB8, PB9); // Crea un Objeto LCD (lcd). Parametros: (rs, enable, d4, d5, d6, d7)
long duracion; //Duracion de tipo long (admite mas bits que el int)
Servo Servomotor;  // construyo un objeto de tipo servo que se llama Servodelospibes

void mostrarDistancia ()
{
  calcularDistanciaEnM(); //lamo a la funcion
  calcularDistanciaEnCm(); //lamo a la funcion
  modoManual(); //lamo a la funcion
  modoAutomatico(); //lamo a la funcion

  lcd.clear(); // limpio el lcd
  delay(10);
  lcd.setCursor(0, 0); // seteo el primer caracter que voy a escribir en la columna 0 y en la fila 1
  lcd.print("Distancia en");  // escribo en la pantalla, lo que dice entre comillas con un maximo de 16 caracteres contando los espacios
  lcd.setCursor(0,1);  // seteo el primer caracter que voy a escribir en la columna 0 y en la fila 2
  lcd.print("metros"); // escribo en la pantalla, lo que dice entre comillas con un maximo de 16 caracteres contando los espacios
  delay(3000);
  lcd.clear();  // limpio el lcd
  lcd.setCursor(0, 0); // seteo el primer caracter que voy a escribir en la columna 0 y en la fila 1
  lcd.print(calcularDistanciaEnM()); // imprimo el valor que me devuelve la funcion calcularDistanciaEnM
  delay(5000);
  lcd.setCursor(0, 0);
  lcd.print("Distancia en");
  lcd.setCursor(0,1);
  lcd.print("centimetros");
  delay(3000);
  lcd.setCursor(0, 0);
  lcd.print(calcularDistanciaEnCm());
  delay(5000);
  lcd.setCursor(0, 0);
  lcd.print("Angulo: ");
  lcd.setCursor(0,1);
  lcd.print(modoManual(), modoAutomatico()); // imprimo el valor que me devuelve la funcion modoManual seguido de modoAutomatico
  delay(3000);
}

/*  int getVariable()
{
   return(seleccion_modo);
}

void setVariable(int _seleccion_modo)
{
   seleccion_modo = _seleccion_modo;
}
*/

// Interrupcion que salta al void ajustes para la configuracion del sistema.
void MenuConfiguraciones()
{
  delayMicroseconds(100);  // pongo un delay ya que puede tardar cierto tiempo en inicializarse para que no haya problemas luego
  const int subir = 2;  //defino una variable entera de tipo const int para solo lectura
  const int bajar = 5 ;  //defino una variable entera de tipo const int para solo lectura
  const int aceptar = 7;  //defino una variable entera de tipo const int para solo lectura
  const int cancelar = 10 ;  //defino una variable entera de tipo const int para solo lectura

  int arriba = digitalRead(subir); // iguala las variables para poder leer en que estado se encuentran
  int abajo = digitalRead(bajar);
  int guardar = digitalRead(aceptar);
  int salir = digitalRead(cancelar);

  String seleccion[]{"Automatico", "Manual"}; // secuencia de caracteres guardados como una matriz de caracteres
  int modo_anterior = seleccion_modo; // igualo a modo anterior con el valor que tome  seleccion_modo para que se convierta en el modo_anterior

  lcd.setCursor(0, 0);  // empiezo en el primer caracter
  lcd.print("A continuacion");  // muestre la palabra seleccione modo
  lcd.setCursor(0, 1);  // empiezo en el primer caracter
  lcd.print("Elija el modo");  // muestre la palabra seleccione modo
  delay(3000);
  //lcd.clear();

    do  //se cumple el do hasta que el while sea ''falso''
    {

      if (abajo == 1) // si abajo almacena un 1
      {
        seleccion_modo = 1000;  //le doy el valor 1000 a seleccion_modo

        if (seleccion_modo == 1000)  // como seleccion_modo tiene el valor 1000
        {
          seleccion_modo = 901;  // seleccion modo pasa a almacenar  901 por lo que se ejecuta el modo automatico
        }

        lcd.clear();
        lcd.setCursor(0, 0);
        lcd.print("Modo:"); // muestra la palabra modo: en el primer renglon
        lcd.setCursor(0, 1);
        lcd.print(seleccion[seleccion_modo]);  // muesta el modo seleccionado
      }

      else if(arriba == 1)  // si arriba recibe un 1
      {
         seleccion_modo = 2000; // saleccion_modo pasa a tomar el valor 2000

         if (seleccion_modo == 2000) // como el valor de seleccion_modo pasa a ser 2000
         {
           seleccion_modo = 900; // se ejecuta el modo manual 
         }

         lcd.clear();
         lcd.setCursor(0, 0);
         lcd.print("Modo:"); // muestra la palabra modo: en el primer renglon
         lcd.setCursor(0, 1);
         lcd.print(seleccion[seleccion_modo]); // muesta el modo seleccionado en el segundo renglon
      }

      else if (salir == 1) // si salir toma el valor de 1
      {
        seleccion_modo = modo_anterior; // hago que seleccion_modo tome el valor de modo_anterior
        lcd.setCursor(0, 0);  // empiezo en el primer caracter
        lcd.print("Cancelado");  // muestre la palabra seleccione modo
        lcd.setCursor(0, 1);  // empiezo en el primer caracter
        lcd.print("Correctamente");  // muestre la palabra seleccione modo
      }

      else if(guardar == 1) // si guardar toma el valor de 1
      {
        seleccion_modo = seleccion_modo; // hago que seleccion_modo siga tomando el valor que tiene
        lcd.setCursor(0, 0);  // empiezo en el primer caracter
        lcd.print("Seleccionado");  // muestre la palabra seleccione modo
        lcd.setCursor(0, 1);  // empiezo en el primer caracter
        lcd.print("Correctamente");  // muestre la palabra seleccione modo
      }
    }

    while (salir == 0 && guardar == 0);// mientras esto se cumpla, sino, cuando se deje de presionar el pulsador, se va a fijar en que modo quedo
    {
      if (seleccion_modo == 901) // si seleccion de modo termiono con el valor 901
        {
          modoAutomatico();  // se ejecuta lo que dice la funcion modoAutomatico
        }

        else if (seleccion_modo == 900) // si seleccion de modo termina con el valor 900
        {
          modoManual(); // se ejecuta lo que dice la funcion modoManual
        }
    }

      mostrarDistancia(); // llamo a la funcion mostrarDistancia para que se ejecute
}


float modoManual(void)
{
   int joyX = PB0;  //Pin analogico para el eje x del joystick
   int servoValx; //Valor entero de el servo en el eje x
   int servoVal; // defino una vatiable de tipo entera para servoval

   servoVal = analogRead(joyX);  // leer el valor analogico del eje x y almacenarlo en servoval
   servoValx = servoVal * 179 / 1023; // regla de 3 para pasar de voltage a grados
   Servomotor.write(servoValx); //Recibo como argumento el valor que almacena ServoValx del joystick
   delay(15);
   return servoValx; // me devuelve el valor del servovalx
}

 // Configurar una funcion para poder calcular la distancia del objeto medido

 float calcularDistanciaEnCm(void)
 {
   // Variables para  la distancia del objeto medido
   float distanciaencm; // defino como float la variable distanciaencm
   // Defino los pines del trigger y del echo
   const int trigPin = 12; // Pin emisor  const para solo lectura
   const int echoPin = 13; // Pin receptor  const para solo lectura
   digitalWrite(trigPin, LOW); // Pongo al emisor en bajo
   delayMicroseconds(2);
   digitalWrite(trigPin, HIGH); // Lo pongo en alto para que envie la señal de sonido
   delayMicroseconds(10);
   digitalWrite(trigPin, LOW); // Lo vuelvo a poner en bajo para que deje de enviar la señal
   duracion = pulseIn(echoPin, HIGH); // Poner echo en alto para poder leer la señal mandada por el trigger y que eso se iguale a la duracion de tipo long
   distanciaencm= 0.034 * (duracion/2); // 0.034 = Velociad del sonido ---> /2 porque es la mitad del recorrido para saber en que momento choco con un objeto
   return distanciaencm; // que me devuelva la distancia de tipo int
 }

 int lectura_joystick(void)
 {
   const int pinJoyX = PB0;  //declaro a el pin analogico como el pin del eje x tipo const int
   int Xvalor; //declaro una variable de tipo entero para que almacene el valor del pinJoyX que entrega el pin analogico
   Xvalor = analogRead(pinJoyX);  //igualo la variable xvalor a la lectura del pinJoyX
   delay(50);
   return Xvalor;  // me devuelve el va´+lor que almaceno Xvalor
 }


 int modoAutomatico()
 {
   int i = 0; // declaro un numero "i" y lo inicializo en 0

   for(int i=0;i < 180; i++)
   {
     Servomotor.write(i); // mueve el servo a i grados ya que write recibe como argumento, un valor del 0 al 179
     delay(10); // delay de lectura
   }

   // La vuelta para el servomotor
   for(int i=179;i >= 0; i--)
   {
     Servomotor.write(i); // mueve el servo a i grados ya que write recibe como argumento, un valor del 179 al 0
     delay(30); // delay de lectura
   }

   return i; // me devuelve el numero en el que esta i durante el for 
 }

 float calcularDistanciaEnM(void)
 {
   // Variables para la distancia del objeto medido
   int distanciaenm; //Distancia de tipo int (admite menos bits que el long)
   const int trigPin = 12; // Pin emisor  const para solo lectura
   const int echoPin = 13; // Pin receptor  const para solo lectura
   digitalWrite(trigPin, LOW); // Pongo al emisor en bajo
   delayMicroseconds(2);
   digitalWrite(trigPin, HIGH); // Lo pongo en alto para que envie la señal de sonido
   delayMicroseconds(10);
   digitalWrite(trigPin, LOW); // Lo vuelvo a poner en bajo para que deje de enviar la señal
   duracion = pulseIn(echoPin, HIGH); // Poner echo en alto para poder leer la señal mandada por el trigger y que eso se iguale a la duracion de tipo long
   distanciaenm= 0.0003432 * (duracion/2); // /2 porque es la mitad del recorrido para saber en que momento choco con un objeto y 0.0003432 es la velocidad del sonido
   return distanciaenm; // que me devuelva la distancia de tipo int
 }

void prueba_conexion (void)
{
  //Permanezco en un while hasta alcanzar la condición de estar conectado a la red o que se cumpla el timeout
   
  while((WiFi.status() != WL_CONNECTED) && (flag_while == 0)){
    delay(100);
    Serial.println(".");
    contador_segundos ++;
    if (contador_segundos == 60){flag_while = 1;} // Coloco esta condición como timeout = 2s, para que en caso de que no se pueda conectar pueda salir del lazo del while.
  }
  //En caso de que me haya podido conectar a la red de WiFi, ejecuto los serialprint que hay dentro del if
  if(WiFi.status() == WL_CONNECTED){
    Serial.print("ESTADO: ");
    Serial.println(WiFi.status());
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
  }
}