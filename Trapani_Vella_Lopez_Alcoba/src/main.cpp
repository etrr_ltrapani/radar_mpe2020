#include <Arduino.h>  // incluyo la libreria de arduino
#include <LiquidCrystal.h> // incluyo la libreria de el LCD
#include <servo.h> // incluyo la libreria de servomotores
#include <protofunciones.h> // incluyo la libreria creada por nosotros
#include <Keypad.h> //incluyo libreria creada para teclado
//#include <WiFi.h> //incluyo libreria creada para conectar al modulo wifi
#include <ThingerESP32.h> //incluyo libreria creada para conectar con thinger.io
#include <FirebaseESP32.h>


//uso la macro para establecer macros y facilitar la futura modificacion del codigo
//Macros para comunicacion UART
#define Rx PA10
#define Tx PA9
//Macro para componentes (servomotor y buzzer)
#define buzzer  PB12
#define controlservo PB3
//Macros de leds para idenfiticar estados
#define ledestadoManual PA8
#define ledestadoAutomatico PB1
#define ledestadoMenu PB13
#define ledestadoSubiendoInfo PB14
#define ledestadoApagado PB15
//Macros de la Matriz
#define F0 PA0
#define F1 PA1
#define F2 PA2
#define F3 PA3
#define C0 PA4
#define C1 PA5 
#define C2 PA6
#define C3 PA7
//Macro de estados
#define Apagado 0 
#define Sensando 1
#define Menu 2
#define SubiendoInfo 3

//defino las variables para ingresar a mi dispositivo de Thinger.io
const char * user = "ltrapani";
const char * id_device = "ESP8266";
const char * credential = "$n@v23_QO9G5";
//Construyo un objeto de tipo teclado llamado teclado para manejar los metodos
Teclado Teclado;
//Construyo un objeto de tipo LiquidCrystal llamado lcd
LiquidCrystal lcd(PB4, PB5, PB6, PB7, PB8, PB9); // Parametros: (rs, enable, d4, d5, d6, d7)
float distanciaencm1 = calcularDistanciaEnCm(); // defino una variable de tipo float para almacenar y poder usar el valor que me devuelve la funcion calcularDistanciaEnCm
float distanciaenm1 = calcularDistanciaEnM(); // defino una variable de tipo float para almacenar y poder usar el valor que me devuelve la funcion calcularDistanciaEnM
// Defino los pines del trigger y del echo
const int trigPin = PB10; // Pin emisor  
const int echoPin = PB11; // Pin receptor  
// Variables para la duracion y la distancia del objeto medido
long duracion; //Duracion de tipo long (admite mas bits que el int)
int estado = Apagado; //Inicializo el estado en apagado
Servo Servomotor;  // construyo un objeto de tipo servo que se llama Servodelospibes

void setup()
{
  Teclado.interrupcion(F0, F1, F2, F3, C0, C1, C2, C3); //defino la interrupcion para usar el metodo de barrido de columnas 
  lcd.begin(16,2); // Inicializa la interfaz para el LCD screen, determinando sus dimensiones (ancho y alto) (16x2)
  Servomotor.attach(controlservo); //Defino el pin 3 (como el argumento que recibe attach) para controlar el servomotor
  Serial.begin(9600); // Establece la velocidad de datos en bits por segundo para la transmisión de datos en serie (para usa el serial monitor)

  pinMode(trigPin, OUTPUT); //Entrega info
  pinMode(echoPin, INPUT); // Recibe info
  //Pines del teclado matricial
  pinMode(F0, INPUT); 
  pinMode(F1, INPUT); 
  pinMode(F2, INPUT); 
  pinMode(F3, INPUT); 
  pinMode(C0, INPUT);
  pinMode(C1, INPUT);
  pinMode(C2, INPUT); 
  pinMode(C3, INPUT); 
  //Pines de comunicacion UART
  pinMode(Tx, OUTPUT); //Entrega info
  pinMode(Rx, INPUT); //Entrega info
  pinMode(buzzer, OUTPUT); //Pin de buzzer
  pinMode(controlservo, OUTPUT); //Pin del servomotor
  pinMode(PB0, OUTPUT); 
  pinMode(PB4, OUTPUT); 
  pinMode(PB5, OUTPUT); 
  pinMode(PB6, OUTPUT); 
  pinMode(PB7, OUTPUT);  
  pinMode(PB8, OUTPUT); 
  pinMode(PB9, OUTPUT); 
  pinMode(PB10, OUTPUT);
  pinMode(PB11, INPUT);
  //Pines de leds para identificar estados
  pinMode(ledestadoAutomatico, OUTPUT); 
  pinMode(ledestadoManual, OUTPUT); 
  pinMode(ledestadoMenu, OUTPUT); 
  pinMode(ledestadoSubiendoInfo, OUTPUT); 
  pinMode(ledestadoApagado, OUTPUT); 

  lcd.setCursor(0, 0);  // empiezo en el primer caracter
  lcd.print("Bienvenido a");  // muestre la palabra
  lcd.setCursor(0, 1);  // empiezo en el primer caracter
  lcd.print("MidienDOW");  // muestre la palabra
  delay(3000);
  lcd.clear();  // limpio el lcd
  lcd.setCursor(0, 0);  // empiezo en el primer caracter
  lcd.print("Nosotros somos:");  // muestre la palabra
  lcd.setCursor(0, 1);  // empiezo en el primer caracter
  lcd.print("Trapani");  // muestre la palabra seleccione modo
  delay(3000);
  lcd.clear();
  lcd.setCursor(0, 0);  // empiezo en el primer caracter
  lcd.print("Vella");  // muestre la palabra
  lcd.setCursor(0, 1);  // empiezo en el primer caracter
  lcd.print("Lopez");  // muestre la palabra
  delay(3000);
  lcd.clear();  //limpia la pantalla
  lcd.setCursor(0, 0);  // empiezo en el primer caracter
  lcd.print("Alcoba");  // muestre la palabra
  delay(3000);
  lcd.clear();

  WiFi.begin("Fibertel WiFi530", "0041705734"); //Ingreso los datos de mi conexion WiFi
  
  prueba_conexion(); //Llamo a la funcion para checkear que no haya ningun error durante el enlace 
  
  thing["Distancia: "] >> [](pson &out){ 
    out = distanciaencm1, distanciaenm1;

}


void loop()
{

switch (estado) //Inicializo a la maquina de estado con la variable de estado y su valor inicial que corresponde al estado "Apagado"
{
//En caso de que el estado sea Apagado
case Apagado: 
digitalWrite (ledestadoApagado, HIGH); //Prendo el led para identificar que esta en apagado
if (Teclado.read() == 4) //Si la funcion de lectura me devuelve un 4
{
  digitalWrite (ledestadoApagado, LOW); //Apago el led para identificar que se prende
  digitalWrite (ledestadoMenu, HIGH); //Prendo el led de menu para identificar que pasara de estado
  estado = Menu; //Hago que pase de estado a menu
}
  break;
//En caso de que el estado sea Menu
case Menu:
/* getVariable(); */
if (ledestadoMenu == HIGH) // Si el led de menu esta encendido (caso positivo), llamo a la funcion para configurar el menu 
{
  MenuConfiguraciones();
}
if (seleccion_modo == 901 || seleccion_modo == 900) //En caso de que esta variable tome el valor de uno de los modos
{
  estado = Sensando; //El programa pasa automaticamente al estado sensando
}

  break;
//En caso de que el estado sea sensando
case Sensando:

/* getVariable(); */
mostrarDistancia(); //Llamo a la funcion para mostrar la distancia
digitalWrite(ledestadoMenu, LOW); //Apago el led de menu
if (seleccion_modo == 0) //En caso de que el modo seleccionado sea automatico
{
  digitalWrite(ledestadoAutomatico, HIGH); //Prendo el led para identificar que el modo es el correcto
  digitalWrite(ledestadoManual, LOW); //Procuro tener apagado el led del otro modo

  if(distanciaencm1 >= 20 && distanciaencm1 < 70)  // si, la distancia que mido es igua o mayor a 20 y a su vez menor a 70, el buzzer , recibe una frecuencia determinada
{
  tone(buzzer, 170); 
  delay(600);
}
if(distanciaencm1 >= 70 && distanciaencm1 < 130)  // si se dan las dos condiciones, tambien recibe una frecuencia, pero esta es un poco mayor
{
  tone(buzzer, 320);
  delay(600);
}
if(distanciaencm1 >= 130 && distanciaencm1 < 200) // mayor frecuencia
{
  tone(buzzer, 600);
  delay(600);
}

if(distanciaenm1 >= 0.2 && distanciaenm1 < 0.7)
{
  tone(buzzer, 170);
  delay(600);
}
if(distanciaenm1 >= 0.7 && distanciaenm1 < 1.3)
{
  tone(buzzer, 320);
  delay(600);
}
if(distanciaencm1 >= 1.3 && distanciaencm1 < 2)
{
  tone(buzzer, 600);
  delay(600);
}
}
else if (seleccion_modo == 1) //En caso de que seleccion modo en este caso, sea el Manual
{
  digitalWrite (ledestadoManual, HIGH); //Prendo el led manual para asegurarme de que el modo fue elegido correctamente
  digitalWrite(ledestadoAutomatico, LOW); //Apago el led del otro modo

  if(distanciaencm1 >= 20 && distanciaencm1 < 70)  // si, la distancia que mido es igua o mayor a 20 y a su vez menor a 70, el buzzer , recibe una frecuencia determinada
{
  tone(buzzer, 170);
  delay(600);
}

if(distanciaencm1 >= 70 && distanciaencm1 < 130)  // si se dan las dos condiciones, tambien recibe una frecuencia, pero esta es un poco mayor
{
  tone(buzzer, 320);
  delay(600);
}
if(distanciaencm1 >= 130 && distanciaencm1 < 200) // mayor frecuencia
{
  tone(buzzer, 600);
  delay(600);
}

if(distanciaenm1 >= 0.2 && distanciaenm1 < 0.7)
{
  tone(buzzer, 170);
  delay(600);
}
if(distanciaenm1 >= 0.7 && distanciaenm1 < 1.3)
{
  tone(buzzer, 320);
  delay(600);
}
if(distanciaencm1 >= 1.3 && distanciaencm1 < 2)
{
  tone(buzzer, 600);
  delay(600);
}
}
if (Teclado.read() == 8) //En caso de que la lectura de la matriz me devuelva un 8
{
  digitalWrite(ledestadoAutomatico, LOW); 
  digitalWrite(ledestadoManual, LOW);
  digitalWrite(ledestadoMenu, HIGH); //Pasa de un estado cualquiera a el menu
  estado = Menu; // El estado se iguala al de menu para ejecutar lo que haya en case Menu:
}

if (Teclado.read() == 16) //En caso de que la lectura de la matriz me devuelva un 16
{
{
  digitalWrite(ledestadoMenu, LOW);
  estado = SubiendoInfo; // Se pasa al caso de SubiendoInfo 
}
break;
//En caso de que el menu sea subiendoinfo
case SubiendoInfo:
digitalWrite(ledestadoSubiendoInfo, HIGH); //prendo el led para identificar que el circuito esta mandando informacion a traves del modulo Wifi

thing.handle(); //Me conecto con el servidor de thinger.io
//Muestro los valores almacenados de distancia en ambas unidades
Serial.println("La distancia en CM es: ");
Serial.println(distanciaencm1);
Serial.println("/n");
Serial.println("La distancia en M es: ");
Serial.println(distanciaenm1);

thing.write_bucket("dist_id", "dist");


break;
}

}