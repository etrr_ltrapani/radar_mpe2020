#include <Arduino.h>
#include <LiquidCrystal.h>
#include <Servo.h>
#include <Keypad.h>


int seleccion_modo = 0;  //define  seleccion_modo como tipo entero

// PROTOTIPOS DE FUNCIONES A IMPLEMENTAR

void mostrarDistancia (void);

float modoManual(void);

float calcularDistanciaEnCm(void);

float calcularDistanciaEnM(void);

int lectura_joystick(void);

int modoAutomatico(void);

void MenuConfiguraciones(void);

void chequeo_conexion (void);

int getVariable(void);

void setVariable(int);

#ifndef Teclado_h
#define Teclado_h

//Creo una clase llamada teclado
class Teclado{
    public: //Creo los metodos que usare mas adelante
        Teclado();
        void interrupcion(uint32_t, uint32_t, uint32_t, uint32_t, uint32_t, uint32_t, uint32_t, uint32_t);
        int getFila();
        int read();
    private: //Creo los atributos para identificar las filas y columnas
        uint32_t F0;
        uint32_t F1;
        uint32_t F2;
        uint32_t F3;
        uint32_t C0;
        uint32_t C1;
        uint32_t C2;
        uint32_t C3;
        int fila=1;
};


//Inicializo las variables con el constructor
void Teclado::interrupcion(uint32_t _f0, uint32_t _f1, uint32_t _f2, uint32_t _f3, uint32_t _c0, uint32_t _c1, uint32_t _c2, uint32_t _c3)
{
    F0 = _f0;
    F1 = _f1;
    F2 = _f2;
    F3 = _f3;
    C0 = _c0;
    C1 = _c1;
    C2 = _c2;
    C3 = _c3;
}

// Creo un get por si lo necesito
int Teclado::getFila()
{
    return fila;
}

//Dentro del metodo read establezco un barrido por columnas para identificar en que fila fue presionado un boton
int Teclado::read()
{
    digitalWrite(F3, 0);
    digitalWrite(F0, 1);
    fila=1; //En la fila uno, hay tres columnas
    if(digitalRead(C0)==1){return 1;} //En caso de que la C0 F1 sea presionada (numero 1 en el teclado), me retorna un 1
    else if(digitalRead(C1)==1){return 2;} //En caso de que la C1 F1 sea presionada (numero 1 en el teclado), me retorna un 2 y asi sucesivamente 
    else if(digitalRead(C2)==1){return 3;}
    else if(digitalRead(C3)==1){return 4;}
    digitalWrite(F0, 0);
    digitalWrite(F1, 1);
    fila=2;
    if(digitalRead(C0)==1){return 5;}
    else if(digitalRead(C1)==1){return 6;}
    else if(digitalRead(C2)==1){return 7;}
    else if(digitalRead(C3)==1){return 8;}
    digitalWrite(F1, 0);
    digitalWrite(F2, 1);
    fila=3;
    if(digitalRead(C0)==1){return 9;}
    else if(digitalRead(C1)==1){return 10;}
    else if(digitalRead(C2)==1){return 11;}
    else if(digitalRead(C3)==1){return 12;}
    digitalWrite(F2, 0);
    digitalWrite(F3, 1);
    fila=4;
    if(digitalRead(C0)==1){return 13;}
    else if(digitalRead(C1)==1){return 14;}
    else if(digitalRead(C2)==1){return 15;}
    else if(digitalRead(C3)==1){return 16;}
}

#endif